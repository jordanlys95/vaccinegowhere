"use strict";
class Loading {
    onAdd(map) {
        this._map = map;
        const container = document.createElement('div')
        container.className = 'lds-dual-ring'
        this._container = container;
        return this._container;
    }
}

class PitchControl {
    onAdd(map) {
        this._map = map;
        const container = document.createElement('div');
        container.className = 'mapboxgl-ctrl mapboxgl-ctrl-group';
        container.innerHTML = '<button class="mapboxgl-ctrl-icon mapboxgl-ctrl-custom-pitch" type="button"><div>3D</div></button>';
        container.onclick = function () {
            var pitch = map.getPitch();
            var nextPitch = 0;
            if (pitch < 30) {
                nextPitch = 30;
            } else if (pitch < 45) {
                nextPitch = 45;
            } else if (pitch < 60) {
                nextPitch = 60;
            }
            map.easeTo({ pitch: nextPitch });
        };
        map.on('pitchend', this.onPitch.bind(this));
        this._container = container;
        return this._container;
    }
    onPitch() {
        const pitch = this._map.getPitch();
        const text = this._container.getElementsByTagName('div')[0];
        text.style.transform = 'rotate3d(1,0,0,' + pitch + 'deg)';
    }
    onRemove() {
        this._container.parentNode.removeChild(this._container);
        this._map.off('pitchend', this.onPitch.bind(this));
        this._map = undefined;
    }
};

class DdlVacControl {
    setVaccineType(vaccineType) {
        this._vaccineType = vaccineType;
    }
    onAdd(map) {
        this._map = map;
        this._options = this._vaccineType.map((type, index) => {
            return `<option ${index === 0 ? 'selected' : ''} value="${type}">${type.toUpperCase()}</option>`
        })
        const container = document.createElement('div');
        container.className = 'mapboxgl-ctrl mapboxgl-ctrl-group vaccine-type--dropdownlist';
        container.innerHTML = `<select onChange="ddlUpdateVacFilter(event)">${this._options}</select>`;
        this._container = container;
        return this._container;
    }
    onRemove() {
        this._container.parentNode.removeChild(this._container);
        this._map = undefined;
    }
};
class VCCounter {
    onAdd(map) {
        this._map = map;
        // this._vcFeatures = map.querySourceFeatures('vaccination-centres');
        const container = document.createElement('div');
        container.id = 'vc-count--container'
        container.className = 'mapboxgl-ctrl';
        container.innerHTML = `
                    <h2 id="vc-count">0</h2>
                    <div>centres</div>
                `;
        container.hidden = true;
        this._container = container;
        return this._container;
    }
    onRemove() {
        this._container.parentNode.removeChild(this._container);
        this._map = undefined;
    }
};
class Logo {
    onAdd(map) {
        this._map = map;
        const container = document.createElement('div');
        container.id = 'logo'
        container.className = 'mapboxgl-ctrl';
        container.innerHTML = `<img height="52px" src="assets/vaccinegowhere--logo-square.svg"/>`;
        this._container = container;
        return this._container;
    }
    onRemove() {
        this._container.parentNode.removeChild(this._container);
        this._map = undefined;
    }
};

mapboxgl.accessToken = 'pk.eyJ1Ijoiam9yZGFubHlzOTUiLCJhIjoiY2tuaTVuYjZ5MzYzaDJxdGFwMDl4MnRqZyJ9.JO_KqMQpP3gHiUj5WvBHoA';
let map

window.addEventListener('load', (event) => {

    const legend = document.getElementById('legend');
    const legendBackdrop = document.getElementById('legend-backdrop');

    const infoButton = document.getElementById('info-button');
    const legendClose = document.getElementById('legend-close');

    infoButton.onclick = (e) => {
        e.preventDefault();
        legend.hidden = false
        legendBackdrop.hidden = false
    };
    infoButton.onmouseover = (e) => {
        e.preventDefault();
        legend.hidden = false;
        legendBackdrop.hidden = false;
    };
    legendClose.onclick = (e) => {
        e.preventDefault();
        legend.hidden = true;
        legendBackdrop.hidden = true;
    };
    legendBackdrop.onclick = (e) => {
        e.preventDefault();
        legend.hidden = true;
        legendBackdrop.hidden = true;
    };

    map = new mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/mapbox/light-v10',
        center: [103.8198, 1.3521],
        zoom: 10,
        attributionControl: false
    });

    map.on('style.load', mapStyleLoaded);

    map.on('click', dismissInfo)

    map.on('load', function () {
        const mapboxGlTopContainer = document.createElement('div')
        mapboxGlTopContainer.className = 'mapboxgl-ctrl-top'
        document.querySelector('.mapboxgl-map .mapboxgl-control-container').prepend(mapboxGlTopContainer)

        map.addLayer(
            {
                id: "bbox",
                type: "fill",
                source: {
                    type: "geojson",
                    tolerance: 10,
                    buffer: 0,
                    data: { "type": "Feature", "properties": {}, "geometry": { "type": "Polygon", "coordinates": [[[-180, 90], [180, 90], [180, -90], [-180, -90], [-180, 90]], [[103.565, 1.475], [104.13, 1.475], [104.13, 1.156], [103.565, 1.156], [103.565, 1.475]]] } },
                },
                paint: {
                    "fill-color": "rgba(255,255,255,.75)",
                    "fill-antialias": false,
                },
            }
        );

        map.addLayer({
            id: 'user-isochrone-outline',
            type: "line",
            source: "user-isochrone-poly",
            'minzoom': 12,
            paint: {
                'line-color': '#2962FF',
                'line-width': 1
            },
        }, getLabelLayerId());
        map.addLayer({
            id: 'user-isochrone-fill',
            type: "fill",
            'minzoom': 12,
            source: "user-isochrone-poly",
            paint: {
                'fill-color': '#E1F5FE',
                'fill-opacity': 0.5
            }
        }, getLabelLayerId());

        map.addLayer(
            {
                'id': '3d-buildings',
                'source': 'composite',
                'source-layer': 'building',
                'filter': ['==', 'extrude', 'true'],
                'type': 'fill-extrusion',
                'minzoom': 15,
                'paint': {
                    'fill-extrusion-color': '#ccc',

                    // Use an 'interpolate' expression to
                    // add a smooth transition effect to
                    // the buildings as the user zooms in.
                    'fill-extrusion-height': [
                        'interpolate',
                        ['linear'],
                        ['zoom'],
                        15,
                        0,
                        15.05,
                        ['get', 'height']
                    ],
                    'fill-extrusion-base': [
                        'interpolate',
                        ['linear'],
                        ['zoom'],
                        15,
                        0,
                        15.05,
                        ['get', 'min_height']
                    ],
                    'fill-extrusion-opacity': 0.6
                }
            },

            getLabelLayerId()
        );

        map.addLayer({
            id: 'selected-vc-points',
            type: "circle",
            source: "vaccination-centres",
            'minzoom': 8,
            paint: {
                "circle-color": [
                    'match',
                    ["to-string", ['get', 'earliestSlot']],
                    '',
                    '#fff',
                    '#A7FFEB'
                ],
                "circle-radius": [
                    'step',
                    ['zoom'],
                    3, 12,
                    8, 14,
                    12
                ],
                "circle-stroke-color": "#0D47A1",
                "circle-stroke-opacity": 1,
                "circle-stroke-width": 4,
                "circle-pitch-alignment": "map"
            },
            filter: ['==', 'vaccineType', 'Pfizer/Comirnaty']
        }, getLabelLayerId());

        map.addLayer({
            'id': 'isochrone-label',
            'type': 'symbol',
            'source': 'user-isochrone-centroid',
            'minzoom': 13,
            'layout': {
                'text-field': ['get', 'description'],
                'text-allow-overlap': true
            },
            'paint': {
                'text-translate': [0, 48],
                'text-color': '#2962FF'
            }
        }, getLabelLayerId());
    });

    map.on('click', 'selected-vc-points', mapClick)

    // Change the cursor to a pointer when the mouse is over the places layer.
    map.on('mouseenter', 'places', function () {
        _map.getCanvas().style.cursor = 'pointer';
    });

    // Change it back to a pointer when it leaves.
    map.on('mouseleave', 'places', function () {
        _map.getCanvas().style.cursor = '';
    });

});

function mapStyleLoaded() {
    //init source data
    map.addSource('vaccination-centres', {
        'type': 'geojson',
        'data': turf.featureCollection([])
    });

    updateVaccinationCentres()

    map.addSource('user-isochrone-poly', {
        'type': 'geojson',
        'data': turf.featureCollection([])
    });
    map.addSource('user-isochrone-centroid', {
        'type': 'geojson',
        'data': turf.featureCollection([])
    });

    var geolocate = new mapboxgl.GeolocateControl({
        positionOptions: {
            enableHighAccuracy: true
        },
        trackUserLocation: true
    });

    geolocate.on('geolocate', setUserIsochrone);
    geolocate.on('trackuserlocationstart', setUserIsochrone);
    geolocate.on('trackuserlocationend', emptyUserIsochrone);
    geolocate.on('error', emptyUserIsochrone);

    map.addControl(geolocate, 'bottom-right');
    map.addControl(new mapboxgl.NavigationControl(), 'bottom-left');
    map.addControl(new PitchControl(), 'bottom-left');
    map.addControl(new VCCounter(), 'top-right');
    map.addControl(new Loading(), 'top-right');
    map.addControl(new Logo(), 'top-left');
    map.addControl(
        new MapboxGeocoder({
            accessToken: mapboxgl.accessToken,
            mapboxgl: mapboxgl,
            countries: 'sg',
            bbox: [103.565, 1.156, 104.13, 1.475]
        }), 'top-left'
    );

}

function mapClick(e) {
    const vcInfo = document.getElementById('vc-info');
    var coordinates = e.features[0].geometry.coordinates.slice();

    map.flyTo({
        offset: [0, 50],
        pitch: 60,
        center: coordinates,
        zoom: 17,
        essential: true // this animation is considered essential with respect to prefers-reduced-motion
    });

    var { name, address, vaccineType, earliestSlot, fees, contact } = e.features[0].properties;

    var earliestSlotElem = (earliestSlot && earliestSlot !== 'null') ? `<strong class="earliest-slot">Available from ${moment(earliestSlot).format('DD MMM, ddd')}</strong><br />` : ''
    var fees = (fees && fees !== 'null' && fees !== 'undefined') ? `<strong>Fee $${parseFloat(fees).toFixed(2)}</strong>` : ''
    var contact = (contact && contact !== 'null' && contact !== 'undefined') ? `<strong><a href='tel:${contact}'>${contact}</a></strong>` : ''
    var description = `
            
            <h1>${vaccineType}</h1>
            ${earliestSlotElem}
            <h3>${name}</h3>
            <p>
                <a target="_blank" href="https://maps.apple.com/?daddr=${coordinates[1]},${coordinates[0]}">${address}</a>
            </p>
            
        `;
    document.getElementById("vc-info").innerHTML = description;
    // Ensure that if the map is zoomed out such that multile
    // copies of the feature are visible, the popup appears
    // over the copy being pointed to.
    while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
        coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
    }

    document.querySelector('.mapboxgl-ctrl-top').hidden = true
    document.querySelector('.mapboxgl-ctrl-top-left').hidden = true
    document.querySelector('.mapboxgl-ctrl-top-right').hidden = true

    vcInfo.hidden = false

}

function updateVaccinationCentres() {
    fetch('https://vaccinegowhere-backend-jordanlys95.vercel.app/api/vaccine/shot', { method: 'POST' })
        .then(response => response.json())
        .then((featureCollection) => {
            map.getSource('vaccination-centres').setData(featureCollection)
            let vaccineType = getType(featureCollection)
            let vaccineCtl = new DdlVacControl()
            vaccineCtl.setVaccineType(vaccineType)
            map.addControl(vaccineCtl, 'top-left');

            document.querySelector('#vc-count--container').hidden = false
            document.querySelector('.lds-dual-ring').hidden = true

            let count = 0
            for(var i = 0; i < featureCollection.features.length; i++){
                document.getElementById("vc-count").innerText = count++
            }
            console.log(vaccineType)
        })
}

function getLabelLayerId() {
    var layers = map.getStyle().layers;
    var labelLayerId;
    for (var i = 0; i < layers.length; i++) {
        if (layers[i].type === 'symbol' && layers[i].layout['text-field']) {
            labelLayerId = layers[i].id;
            break;
        }
    }
    return labelLayerId
}

function btnNADclose(e) {
    document.querySelector('.NAD-container').hidden = true;
}

function toggleLayer({ target }) {
    if (layerExist(target.value)) {
        hideAllVaccineTypes()
        map.setLayoutProperty(
            target.value,
            'visibility',
            (target.checked) ? 'visible' : 'none'
        );

        map.flyTo({
            pitch: 0,
            center: [103.8198, 1.3521],
            zoom: 10,
            essential: true // this animation is considered essential with respect to prefers-reduced-motion
        });

    }
}

function layerExist(layer_name) {
    return (typeof map.getLayer(layer_name) !== 'undefined')
}

function setUserIsochrone(e) {
    if (e.coords) {
        var lon = e.coords.longitude;
        var lat = e.coords.latitude
        var position = [lon, lat];

        fetch(`https://api.mapbox.com/isochrone/v1/mapbox/walking/${position.join(",")}.json?contours_minutes=10&polygons=true&access_token=${mapboxgl.accessToken}`)
            .then(response => response.json())
            .then(geojson => {
                let { features } = geojson
                let centroid = turf.centroid(features[0])
                centroid.properties = { description: '10 mins Walk' }

                map.getSource('user-isochrone-poly').setData(geojson);
                map.getSource('user-isochrone-centroid').setData(centroid);
            })
    }

}

function emptyUserIsochrone() {
    let empty_collection = turf.featureCollection([])
    map.getSource('user-isochrone-poly').setData(empty_collection);
    map.getSource('user-isochrone-centroid').setData(empty_collection);
}

function dismissInfo() {
    const vcInfo = document.getElementById('vc-info');
    vcInfo.hidden = true

    document.querySelector('.mapboxgl-ctrl-top').hidden = false
    document.querySelector('.mapboxgl-ctrl-top-left').hidden = false
    document.querySelector('.mapboxgl-ctrl-top-right').hidden = false
    map.flyTo({
        offset: [0, -50],
        pitch: 0,
        essential: true // this animation is considered essential with respect to prefers-reduced-motion
    });
}

function getType(featureCollection) {
    let vaccineArray = []
    turf.propReduce(featureCollection, function (previousValue, { vaccineType }, featureIndex) {
        vaccineArray.push(vaccineType)
    });
    return vaccineArray.filter(onlyUnique)
}

function onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
}

function ddlUpdateVacFilter({ target }) {
    let type = target.options[target.selectedIndex].value

    map.setFilter('selected-vc-points', ['==', 'vaccineType', type]);

    // reset view to center on singapore
    map.flyTo({
        pitch: 0,
        center: [103.8198, 1.3521],
        zoom: 10,
        essential: true // this animation is considered essential with respect to prefers-reduced-motion
    });

}
