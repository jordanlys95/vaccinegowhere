# Add Website to Mobile Device Home Screen
Follow the instructions below to add a shortcut to a website on the home screen of your iPad, iPhone, or Android devices.

**iPad or iPhone**

 1. Launch "Safari" app.  This does not work from the "Chrome" app.
 2. Tap the "Share" icon featuring a up-pointing arrow coming out of a box along the bottom of the Safari window to open the Share Sheet.
 3. Tap "Add to Home Screen."
 4. The Add to Home dialog box will appear, with the icon that will be used for this website on the left side of the dialog box.
 5. Tap "Add."
 6. Safari will close automatically and you will be taken to where the icon is located on your Home Screen.

**Android**

 1. Launch “Chrome” app. Open the website or web page you want to pin to your home screen. 
 2. Tap the "menu" icon (3 dots in upper right-handcorner) and tap "Add to homescreen." 
 3. You’ll be able to enter a name for the shortcut and then Chrome will add it to your home screen.
